<div class="alert alert-{{$type}} {{$selector}}" role="alert"> 
	@isset($title)
		<strong>{{$title}}</strong> 
	@endisset
	<sapn class='msg'>{{$slot}}</sapn>
</div>