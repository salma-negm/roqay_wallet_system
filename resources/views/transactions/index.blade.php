<div class="row">
  <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
          <div class="panel-heading">@yield('title')</div>

          <div class="panel-body">
              @component('components.balance')
                  {{ Auth::user()->balance }}
              @endcomponent

              <div class="col-xs-12">
                @section('form')
                @show
              </div>
              <hr class="col-xs-12">
              <div class="col-xs-12">
                @section('table')
                @show
              </div>


          </div>
      </div>
  </div>
</div>