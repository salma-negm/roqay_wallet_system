@extends('transactions.index')
@section('title','Deposit')
@section('form')
	@component('components.alert')
	    @slot('type')
	        success
	    @endslot

	    @slot('selector')
	        alert-frm-success hide
	    @endslot

	@endcomponent

	@component('components.alert')
	    @slot('type')
	        danger
	    @endslot

	    @slot('selector')
	        alert-frm-error hide
	    @endslot
	@endcomponent
	<form class="form-inline col-xs-8 col-xs-offset-2" method="post" action="{{ route('store_deposit') }}" id="frm-deposit">
	  {{ csrf_field() }}
	  <div class="form-group col-xs-10">
    	<input type="number" name="amount" class="form-control" id="frm-amount" placeholder="Amount ..." style="width: 100%">
	  </div>
	  <button type="submit" class="btn btn-default">Done</button>
	</form>
@endsection

@section('table')
	@if (count($transactions) > 0)
	    <table class="table table-hover"> 
	        <thead>
	            <tr> 
	                <th>#</th>  
	                <th>Amount</th>
	                <th>Date</th> 
	                <th>Time</th> 
	            </tr>
	        </thead> 
	        <tbody> 
	            @foreach($transactions as $transaction)
	                <tr> 
	                    <th scope="row">{{$loop->iteration}}</th> 
	                    <td>{{$transaction->amount}}</td>
	                    <td>{{$transaction->created_at->format('d-m-Y')}}</td>
                        <td>{{$transaction->created_at->format('H:i:s')}}</td>
	                </tr> 
	            @endforeach
	        </tbody> 
	    </table>
	@else
	    @component('components.alert')
	    	@slot('type')
	    		info
	    	@endslot

	    	@slot('selector')
	    		alert-no-transactions
	    	@endslot

	    	No Transactions
	    @endcomponent
	@endif
@endsection

<script type="text/javascript">
     $('#frm-deposit').on('submit',function(e){
     	e.preventDefault();
     	var $this=$(this);
     	$.ajax({
            url:$this.attr('action'),
            type:$this.attr('method'),
            data:$(this).serialize(),
            success:function(responce){
                var $main_holder=$('.main-content-holder');
                $main_holder.html(responce.html);
                $main_holder.find('.alert-frm-success .msg').text('Operation Done Successfully').parent().removeClass('hide');

                setTimeout(function(){ 
                	$main_holder.find('.alert-frm-success').addClass('hide'); 
                }, 3000	);
                $main_holder.find('.alert-frm-error').addClass('hide');
            },
            error:function(e,json, errorThrown){
                var $main_holder=$('.main-content-holder');
                var msg='';
                $.each(e.responseJSON,function(key,value){
                    if(value.constructor === Array)
                        $.each(value,function(idx,err){
                            debugger;
                            msg+='<li>'+err+'</li>';
                        });
                    else
                        msg+='<li>'+err+'</li>';
                });
                if(msg)
                {
                    $main_holder.find('.alert-frm-error .msg').html(msg).parent().removeClass('hide');
                    setTimeout(function(){ 
                    	$main_holder.find('.alert-frm-error').addClass('hide'); 
                    }, 3000	);
                    $main_holder.find('.alert-frm-success').addClass('hide');
                }
            }

        });
    });
 </script>
