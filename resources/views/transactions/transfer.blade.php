@extends('transactions.index')
@section('title','Transfer')
@section('form')
	@component('components.alert')
	    @slot('type')
	        success
	    @endslot

	    @slot('selector')
	        alert-frm-success hide
	    @endslot

	@endcomponent

	@component('components.alert')
	    @slot('type')
	        danger
	    @endslot

	    @slot('selector')
	        alert-frm-error hide
	    @endslot
	@endcomponent
	<form class="form-inline" method="post" action="{{ route('store_transfer') }}" id="frm-deposit">
	  {{ csrf_field() }}
	  <div class="form-group col-xs-5">
    	<input type="text" name="email" class="form-control" id="frm-email" placeholder="Recipient E-Mail ..." style="width: 100%">
	  </div>
	  <div class="form-group col-xs-5">
    	<input type="number" name="amount" class="form-control" id="frm-amount" placeholder="Amount ..." style="width: 100%">
	  </div>
	  <button type="submit" class="btn btn-default">Done</button>
	</form>
@endsection

@section('table')
	@if (count($transactions) > 0)
	    <table class="table table-hover"> 
	        <thead>
	            <tr> 
	                <th>#</th>
	                <th>From</th>
	                <th>To</th>  
	                <th>Amount</th>
	                <th>Date</th> 
	                <th>Time</th> 
	            </tr>
	        </thead> 
	        <tbody> 
	            @foreach($transactions as $transaction)
	                <tr> 
	                	<th scope="row">{{$loop->iteration}}</th> 
	                	<td>{{$transaction->creator->name}}</td>
	                	<td>{{$transaction->recipient->name}}</td>
	                    <td>{{$transaction->amount}}</td>
	                    <td>{{$transaction->created_at->format('d-m-Y')}}</td>
                        <td>{{$transaction->created_at->format('H:i:s')}}</td>
                        
	                </tr> 
	            @endforeach
	        </tbody> 
	    </table>
	@else
	    @component('components.alert')
	    	@slot('type')
	    		info
	    	@endslot

	    	@slot('selector')
	    		alert-no-transactions
	    	@endslot

	    	No Transactions
	    @endcomponent
	@endif
@endsection

<script type="text/javascript">
     $('#frm-deposit').on('submit',function(e){
     	e.preventDefault();
     	var $this=$(this);
     	$.ajax({
            url:$this.attr('action'),
            type:$this.attr('method'),
            data:$(this).serialize(),
            success:function(responce){
            	debugger;
                var $main_holder=$('.main-content-holder');
                $main_holder.html(responce.html);
                $main_holder.find('.alert-frm-success .msg').text('Operation Done Successfully').parent().removeClass('hide');

                setTimeout(function(){ 
                	$main_holder.find('.alert-frm-success').addClass('hide'); 
                }, 3000	);
                $main_holder.find('.alert-frm-error').addClass('hide');
            },
            error:function(e,json, errorThrown){
            	debugger;

                var $main_holder=$('.main-content-holder');
                //$main_holder.html(e.responseText);
                var msg='';
                $.each(e.responseJSON,function(key,value){
                    if(value.constructor === Array)
                    {
                    	console.log(value);
                        $.each(value,function(idx,err){
                            debugger;
                            msg+='<li>'+err+'</li>';
                        });
                    }
                    else
                        msg+='<li>'+err+'</li>';
                });
                if(msg)
                {
                    $main_holder.find('.alert-frm-error .msg').html(msg).parent().removeClass('hide');
                    setTimeout(function(){ 
                    	$main_holder.find('.alert-frm-error').addClass('hide'); 
                    }, 3000	);
                    $main_holder.find('.alert-frm-success').addClass('hide');
                }
            }

        });
    });
 </script>
