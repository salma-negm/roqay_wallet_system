@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <!-- <div class="panel-heading">Dashboard</div> -->

                <div class="panel-body">
                    @component('components.balance')
                        {{ Auth::user()->balance }}
                    @endcomponent

                    <div class="btn-group btn-group-justified btns-transactions" role="group" aria-label="...">
                      <div class="btn-group" role="group">
                        <button type="button" class="btn btn-info" data-url="{{ route('create_deposit') }}">
                            Deposit
                        </button>
                      </div>
                      <div class="btn-group" role="group">
                        <button type="button" class="btn btn-info" data-url="{{ route('create_withdraw') }}">
                            Withdraw
                        </button>
                      </div>
                      <div class="btn-group" role="group">
                        <button type="button" class="btn btn-info" data-url="{{ route('create_transfer') }}">
                            Transfer
                        </button>
                      </div>
                    </div>
                    <br><br>
                    <div>
                        @if (count($transactions) > 0)
                            <table class="table table-hover"> 
                                <thead>
                                    <tr> 
                                        <th>#</th> 
                                        <th>Type</th> 
                                        <th>Date</th> 
                                        <th>Time</th> 
                                        <th>Amount</th> 
                                        <th>To</th> 
                                    </tr>
                                </thead> 
                                <tbody> 
                                    @foreach($transactions as $transaction)
                                        <tr> 
                                            <th scope="row">{{$loop->iteration}}</th> 
                                            <td>{{$transaction->type->name}}</td>
                                            <td>{{$transaction->created_at->format('d-m-Y')}}</td>
                                            <td>{{$transaction->created_at->format('H:i:s')}}</td>
                                            <td>{{$transaction->amount}}</td>
                                            <td>
                                                @if ($transaction->recipient != $transaction->creator)
                                                    {{$transaction->creator->name}}
                                                @endif
                                            </td>
                                        </tr> 
                                    @endforeach
                                </tbody> 
                            </table>
                        @else
                            @component('components.alert')
                                @slot('type')
                                    info
                                @endslot

                                @slot('selector')
                                    alert-no-transactions
                                @endslot

                                No Transactions
                            @endcomponent
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-scripts')
<script type="text/javascript">
     $('.btns-transactions').find('button').on('click',function(){
        var $this=$(this);
        var url= $this.attr('data-url');
        $.get(url,function(responce){
            $('.main-content-holder').html(responce.html);
        });
    });
 </script>
@endsection    

    

