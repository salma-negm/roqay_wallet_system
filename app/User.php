<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function transactions()
    {
        return $this->hasMany('App\Transaction','from_id')->orderBy('created_at');
    }

    public $rules = [
        'email' => 'required|email|exists:users,email',
        'balance'=>'min:0'
    ];

    public function enogh_balance($input) {
        return Validator::make(['balance'=>$input], $this->rules);
      }
}
