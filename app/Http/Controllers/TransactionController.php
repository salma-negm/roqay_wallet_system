<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Transaction;
use Illuminate\Http\Response;
use Log;
use DB;
use Config;
use Illuminate\Http\JsonResponse;

define('TRANSACTIONS_TYPES', Config::get('dbmigrations.transactions_types'));

class TransactionController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }



    public function create_deposit()
    {
    	$logged_user=Auth::user();
    	$transactions=$logged_user->transactions
                                ->where('type_id','=',TRANSACTIONS_TYPES['DEPOSIT']);
    	$view=view('transactions.deposits',compact('transactions'));
        $html=$view->render();
        return response()->json(['success'=>true,'html'=>$html]);
    }

    public function store_deposit(Request $request)
    {
        $logged_user=Auth::user();
        DB::beginTransaction();
        $transaction = new Transaction;
        $v = $this->validate($request,$transaction->rules);
        try
        {
            $transaction->amount=$request->amount;
            $transaction->type_id=TRANSACTIONS_TYPES['DEPOSIT'];
            $transaction->from_id=$logged_user->id;
            $transaction->to_id=$logged_user->id;
            $transaction->save();

            $logged_user->balance+=$request->amount;  
            $logged_user->save();     

            DB::commit();
        }
        catch (\Exception $e) 
        {
            DB::rollback();
        }
        return redirect()->action('TransactionController@create_deposit');
    }

    public function create_withdraw()
    {
        $logged_user=Auth::user();
        $transactions=$logged_user->transactions
                                ->where('type_id','=',TRANSACTIONS_TYPES['WITHDRAW']);
        $view=view('transactions.withdraw',compact('transactions'));
        $html=$view->render();
        return response()->json(['success'=>true,'html'=>$html]);
    }

    public function store_withdraw(Request $request)
    {
        $logged_user=Auth::user();
        DB::beginTransaction();
        $transaction = new Transaction;
        $v = $this->validate($request,$transaction->rules);
        try
        {
            $transaction->amount=$request->amount;
            $transaction->type_id=TRANSACTIONS_TYPES['WITHDRAW'];
            $transaction->from_id=$logged_user->id;
            $transaction->to_id=$logged_user->id;
            $transaction->save();

            $logged_user->balance-=$request->amount;  
            $logged_user->save();     

            DB::commit();
        }
        catch (\Exception $e) 
        {
            DB::rollback();
        }
        return redirect()->action('TransactionController@create_withdraw');
    }

    public function create_transfer()
    {
        $logged_user=Auth::user();
        $transactions=Transaction::where('type_id','=',TRANSACTIONS_TYPES['TRANSFER'])
                                ->where(function ($query) use ($logged_user){
                                    $query->where('from_id','=',$logged_user->id)
                                          ->orWhere('to_id','=',$logged_user->id);
                                })->get();
        $view=view('transactions.transfer',compact('transactions'));
        $html=$view->render();
        return response()->json(['success'=>true,'html'=>$html]);
    }

    public function store_transfer(Request $request)
    {
        $logged_user=Auth::user();
        
        
        $user=new User;
        $user_validation=$this->validate($request,$user->rules);
        // if(count($user) > 0)
        // {}
        $transaction = new Transaction;
        $v = $this->validate($request,$transaction->rules);
        $user=User::where('email','=',$request->email)->first();
        $this->validate($request,
                [
                    'email'=>'not_in:'.$logged_user->email
                ]
            );
        $logged_user->balance-=$request->amount;
        $user->balance+=$request->amount;
        if($logged_user->balance >= 0)
        {
            // return new JsonResponse(['balance'=>'Not enough balance.'], 422);

        // $from_balance_validation=$this->validate(['balance'=>$logged_user->balance],
        //         [
        //             'balance'=>'min:0'
        //         ]
        //     );

        // $from_balance_validation=Validator::make(
        //         ['balance' => $logged_user->balance],
        //         ['balance' => ['min:0']]
        //     );

        // $from_balance_validation=$logged_user->enogh_balance($logged_user->balance);
        // if($from_balance_validation->passes())
        // {
            DB::beginTransaction();
            try
            {
                
                $transaction->amount=$request->amount;
                $transaction->type_id=TRANSACTIONS_TYPES['TRANSFER'];
                $transaction->from_id=$logged_user->id;
                $transaction->to_id=$user->id;
                $transaction->save();


                  
                $user->save(); 

                  
                $logged_user->save();     

                DB::commit();
            }
            catch (\Exception $e) 
            {
                DB::rollback();
            }
            return redirect()->action('TransactionController@create_transfer');
        // }
        // else
        //     return 'dffffffffffffff';
    }
    }
}
