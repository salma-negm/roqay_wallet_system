<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public function creator()
    {
        return $this->belongsTo('App\User','from_id');
    }

    public function recipient()
    {
        return $this->belongsTo('App\User','to_id');
    }

    public function type()
    {
        return $this->belongsTo('App\TransactionType','type_id');
    }

    public   $rules = [
        'amount' => 'required|integer|min:1'
    ];
}
