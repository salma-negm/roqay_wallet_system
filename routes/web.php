<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/api/balance', 'API@balance')->name('balance_api');

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(array('before' => 'auth'), function()
{
    Route::get('diposits/create/', 'TransactionController@create_deposit')->name('create_deposit');
    Route::post('diposits/', 'TransactionController@store_deposit')->name('store_deposit');

    Route::get('withdrawals/create/', 'TransactionController@create_withdraw')->name('create_withdraw');
    Route::post('withdrawals/', 'TransactionController@store_withdraw')->name('store_withdraw');

    Route::get('transfers/create/', 'TransactionController@create_transfer')->name('create_transfer');
    Route::post('transfers/', 'TransactionController@store_transfer')->name('store_transfer');
});
